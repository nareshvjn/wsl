# wsl
GUI Installer Script

Network workaround
1. Move 01-netcfg.yaml file to /etc/netplan/
2. Run "sudo netplan try" first time only
3. Move wsl_network.sh to ~/Public/
4. Make alias in .zshrc net="sh ~/Public/wsl_network.sh"
5. Then run net alias
