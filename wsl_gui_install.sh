#!/bin/bash

#Guide to Install GNOME gui in wsl2
#https://gist.github.com/tdcosta100/385636cbae39fc8cd0937139e87b1c74

sudo apt update
wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
sudo apt update
sudo wget -O /etc/apt/trusted.gpg.d/wsl-transdebian.gpg https://arkane-systems.github.io/wsl-transdebian/apt/wsl-transdebian.gpg
sudo chmod a+r /etc/apt/trusted.gpg.d/wsl-transdebian.gpg
sudo touch /etc/apt/sources.list.d/wsl-transdebian.list
version=`cat /etc/os-release | awk '{ FS = "=" } ; NR==11 {print$2}'`
cat << EOF | sudo tee /etc/apt/sources.list.d/wsl-transdebian.list
deb https://arkane-systems.github.io/wsl-transdebian/apt/ $version main 
deb-src https://arkane-systems.github.io/wsl-transdebian/apt/ $version main
EOF
sudo apt update
sudo apt install ubuntu-desktop-minimal tigervnc-standalone-server systemd-genie dotnet-runtime-5.0 apt-transport-https -y
vncpasswd
sudo -H vncpasswd
sudo -H -u gdm vncpasswd
sudo mv /usr/bin/Xorg /usr/bin/Xorg_old
sudo cp ~/wsl/Xorg_new /usr/bin/Xorg_new
sudo chown root:root /usr/bin/Xorg_new
sudo chmod 0755 /usr/bin/Xorg_new
sudo ln -sf /usr/bin/Xorg_new /usr/bin/Xorg
cat << EOF | sudo tee -a /etc/gdm3/custom.conf
AutomaticLoginEnable=true
AutomaticLogin=$USER
EOF
genie -s && exit
read -p 'Open VNCviewer in windows and conect to localhost:5901' bye
kill -9 $PPID
