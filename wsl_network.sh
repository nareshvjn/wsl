#!/bin/bash

# Ref: https://stackoverflow.com/questions/61002681/connecting-to-wsl2-server-via-local-network
 
sudo sed -i '4i nameserver 8.8.8.8' /etc/resolv.conf
sudo sed -i '5s/^/# /' /etc/resolv.conf

sudo ip addr flush dev eth0
sudo dhclient eth0
sudo netplan apply
sudo dhclient eth0
